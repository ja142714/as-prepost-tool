from decouple import config
from zeep import Client
from zeep.wsse.username import UsernameToken
   
### The below currently lives in .env with username/password.  It may move to another area as a global variable once introduced with Prisma
nodeName = config('NODENAME')
###

wsdl = 'http://10.155.50.140/ws/17.0/Status.asmx?WSDL'
username = config('USER')
password = config('PASS')
client = Client(wsdl, wsse=UsernameToken(username, password))

NodeSoftwareStatusGet = client.service.NodeSoftwareStatusGet(NodeNameOrId=nodeName)
EnbSonPciStatusGet = client.service.EnbSonPciStatusGet(NodeNameOrId=nodeName)
#Service: NodeSoftwareStatusGet(NodeNameOrId: xsd:string, _soapheaders={Credentials: ns0:Credentials}) -> NodeSoftwareStatusGetResult: ns0:NodeSoftwareGetResult
'''
{
    'ErrorCode': 'OK',
    'ErrorString': None,
    'NodeResult': 'OK',
    'Name': None,
    'NodeId': None,
    'SwList': {
        'SwStatusWs': [
            {
                'ImageType': 'LTE',
                'RunningVersion': '14.17.00.951',
                'StandbyVersion': '14.15.50.1005',
                'LastRequested': 'Activate (Download If Needed)',
                'NmsState': 'Idle',
                'NodeState': 'Idle ()',
                'LastReadFromNode': datetime.datetime(2020, 6, 25, 11, 19, 3, 867000),
                'IsScheduled': None,
                'FactoryVersion': None,
                'BootVersion': None
            }
        ]
    }
}
'''
#Service: EnbSonPciStatusGet(NodeNameOrId: xsd:string, _soapheaders={Credentials: ns0:Credentials}) -> EnbSonPciStatusGetResult: ns0:LteSonPciGetResult
'''
{
    'ErrorCode': 'OK',
    'ErrorString': None,
    'NodeResult': 'OK',
    'Name': 'RSTNVAE2MMOHOO0002501',
    'NodeId': 'D05F07CE0CE4',
    'PciStatus': 'Automatic',
    'Cell': [
        {
            'CellNumber': 1,
            'CellId': 49,
            'PhysicalLayerIdentity': 0,
            'PhysicalCellId': 471,
            'PciStatus': 'Allocation Success',
            'TacSource': None,
            'Tac': None
        },
        {
            'CellNumber': 2,
            'CellId': 57,
            'PhysicalLayerCellGroup': 159,
            'PhysicalLayerIdentity': 0,
            'PhysicalCellId': 477,
            'PciStatus': 'Allocation Success',
            'TacSource': None,
            'Tac': None
        }
    ]
}
'''
def UUT_Config():
    runningSwVersions = (NodeSoftwareStatusGet['SwList']['SwStatusWs'][0]['RunningVersion'])
    print('\n')
    print(nodeName, 'Running Version:', runningSwVersions )

    for group in EnbSonPciStatusGet['Cell']:
        print("Cell: {} ".format(group['CellNumber']))
        print("Cell ID: {} ".format(group['CellId']))
        print("PCI: {} \n".format(group['PhysicalCellId']))
