from decouple import config
from zeep import Client
from zeep.wsse.username import UsernameToken
   
nodeName = 'RSTNVAE2MMOHOO0002501'
dateStart = '2020-06-16T12:00:00'
dateEnd = '2020-06-16T12:15:00'

wsdl = 'http://10.155.50.140/ws/17.0/Statistics.asmx?WSDL'
username = config('USER')
password = config('PASS')
client = Client(wsdl, wsse=UsernameToken(username, password))
PerPLMNGetRaw = client.service.PerPlmnRawGet(NodeName=nodeName, DateStart=dateStart, DateEnd=dateEnd)

#Per-PLMN Statistics
for group in PerPLMNGetRaw['StatisticsResult'][0]['LtePerPlmnStatsRow']:
    
    print("\nPLMN: {}-{} --> Cell {}".format(group['Mcc'], group['Mnc'], group['CellNumber']))
    print("Attach Requests/Success")
    print("Initial Attach Requests: {}".format(group['NumberOfInitialAttachRequest']))
    print("Initial Attach Success: {}".format(group['NumberOfInitialAttachSuccess']))

    print("\nAttach Latency Average/Maximum (ms)")
    print("Attach Latency Avg (ms): {}".format(group['InitialAttachLatencyAvg']))
    print("Attach Latency Max (ms): {}".format(group['InitialAttachLatencyMax']))

    print("\nRRC Connection Establishment Setup Time Mean/Max (ms)")
    print("RRC Mean Setup Time - Emergency (ms): {}".format(group['ConnEstabTimeMeanEmergency']))
    print("RRC Mean Setup Time - HPA (ms): {}".format(group['ConnEstabTimeMeanHighPriorityAccess']))
    print("RRC Mean Setup Time - MT (ms): {}".format(group['ConnEstabTimeMeanMtAccess']))
    print("RRC Mean Setup Time - MO Signalling (ms): {}".format(group['ConnEstabTimeMeanMoSignalling']))
    print("RRC Mean Setup Time - MO Data (ms): {}".format(group['ConnEstabTimeMeanMoData']))
    print("RRC Mean Setup Time - Delay Tolerant (ms): {}".format(group['ConnEstabTimeMeanDelayTolerantAccess']))
    print('\n')
    print("RRC Max Setup Time - Emergency (ms): {}".format(group['ConnEstabTimeMaxEmergency']))
    print("RRC Max Setup Time - HPA (ms): {}".format(group['ConnEstabTimeMaxHighPriorityAccess']))
    print("RRC Max Setup Time - MT (ms): {}".format(group['ConnEstabTimeMaxMtAccess']))
    print("RRC Max Setup Time - MO Signalling (ms): {}".format(group['ConnEstabTimeMaxMoSignalling']))
    print("RRC Max Setup Time - MO Data (ms): {}".format(group['ConnEstabTimeMaxMoData']))
    print("RRC Max Setup Time - Delay Tolerant (ms): {}".format(group['ConnEstabTimeMaxDelayTolerantAccess']))
    print('\n-----------------------------------------------------')

