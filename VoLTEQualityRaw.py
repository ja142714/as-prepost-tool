from decouple import config
from zeep import Client
from zeep.wsse.username import UsernameToken
#import logging.config
   
nodeName = 'RSTNVAE2MMOHOO0002501'
dateStart = '2020-06-12T15:45:00'
dateEnd = '2020-06-12T16:00:00'

wsdl = 'http://10.155.50.140/ws/17.0/Statistics.asmx?WSDL'
username = config('USER')
password = config('PASS')
client = Client(wsdl, wsse=UsernameToken(username, password))

VoLTEQualityRaw = client.service.VolteQualityRawGet(NodeName=nodeName, DateStart=dateStart, DateEnd=dateEnd)

Cell1_DlVoltePkt = (VoLTEQualityRaw['StatisticsResult'][0]['VolteQualityStatsRow'][0]['DlVoltePkt'])
Cell2_DlVoltePkt = (VoLTEQualityRaw['StatisticsResult'][0]['VolteQualityStatsRow'][1]['DlVoltePkt'])

print('Cell 1 DLVolte Packet Total:', Cell1_DlVoltePkt )
print('Cell 2 DLVolte Packet Total:', Cell2_DlVoltePkt )