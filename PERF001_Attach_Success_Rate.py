from decouple import config
from zeep import Client
from zeep.wsse.username import UsernameToken
   
### The below currently lives in .env with username/password.  It may move to another area as a global variable once introduced with Prisma
nodeName = config('NODENAME')
dateStart = config('DATESTART')
dateEnd = config('DATEEND')
###

wsdl = 'http://10.155.50.140/ws/17.0/Statistics.asmx?WSDL'
username = config('USER')
password = config('PASS')
client = Client(wsdl, wsse=UsernameToken(username, password))

PerPLMNRawGet = client.service.PerPlmnRawGet(NodeName=nodeName, DateStart=dateStart, DateEnd=dateEnd)
RrcConnectionRawGet = client.service.RrcConnectionRawGet(NodeName=nodeName, DateStart=dateStart, DateEnd=dateEnd)

def AttachSuccessRate():
    print('======================================================')
    print('RRC Connection Statistics'.center(24))
    print('======================================================')
    ##RRC Connection RAW Statistics
    for group in RrcConnectionRawGet['StatisticsResult'][0]['LteRrcStatsRow']:
        print("\nCell {} -----------------------------------------------".format(group['CellNumber']))
        print("RRC Connection Attempts/Success")
        print("\nRRC Connection Establishment Setup Time Mean/Max RRC Connection Success:")
        print("Emergency RRC Connection Attempts: {}".format(group['ConnEstabAttEmergency']))
        print("HPA RRC Connection Attempts: {}".format(group['ConnEstabAttHighPriorityAccess']))
        print("MT RRC Connection Attempts: {}".format(group['ConnEstabAttMtAccess']))
        print("MO Signalling RRC Connection Attempts: {}".format(group['ConnEstabAttMoSignalling']))
        print("MO Data RRC Connection Atttempts: {}".format(group['ConnEstabAttMoData']))
        print("RRC Connection Attempts - Total: {}".format(group['ConnEstabAttSum']))
        print('\n')
        print("Emergency RRC Connection Success: {}".format(group['ConnEstabSuccEmergency']))
        print("HPA RRC Connection Success: {}".format(group['ConnEstabSuccHighPriorityAccess']))
        print("MT RRC Connection Success: {}".format(group['ConnEstabSuccMtAccess']))
        print("MO Signalling RRC Connection Success: {}".format(group['ConnEstabSuccMoSignalling']))
        print("MO Data RRC Connection Success: {}".format(group['ConnEstabSuccMoData']))
        print("RRC Connection Success - Total: {}".format(group['ConnEstabSuccSum']))

    print('======================================================')
    print('Per PLMN Statistics'.center(24))
    print('======================================================')
    for group in PerPLMNRawGet['StatisticsResult'][0]['LtePerPlmnStatsRow']:
        print("\nPLMN: {}-{} --> Cell {} -----------------------------".format(group['Mcc'], group['Mnc'], group['CellNumber']))
        print("Attach Requests/Success")
        print("Initial Attach Requests: {}".format(group['NumberOfInitialAttachRequest']))
        print("Initial Attach Success: {}".format(group['NumberOfInitialAttachSuccess']))

        print("\nAttach Latency Average/Maximum (ms)")
        print("Attach Latency Avg (ms): {}".format(group['InitialAttachLatencyAvg']))
        print("Attach Latency Max (ms): {}".format(group['InitialAttachLatencyMax']))

        print("\nRRC Connection Establishment Setup Time Mean/Max (ms)")
        print("RRC Mean Setup Time - Emergency (ms): {}".format(group['ConnEstabTimeMeanEmergency']))
        print("RRC Mean Setup Time - HPA (ms): {}".format(group['ConnEstabTimeMeanHighPriorityAccess']))
        print("RRC Mean Setup Time - MT (ms): {}".format(group['ConnEstabTimeMeanMtAccess']))
        print("RRC Mean Setup Time - MO Signalling (ms): {}".format(group['ConnEstabTimeMeanMoSignalling']))
        print("RRC Mean Setup Time - MO Data (ms): {}".format(group['ConnEstabTimeMeanMoData']))
        print("RRC Mean Setup Time - Delay Tolerant (ms): {}".format(group['ConnEstabTimeMeanDelayTolerantAccess']))
        print('\n')
        print("RRC Max Setup Time - Emergency (ms): {}".format(group['ConnEstabTimeMaxEmergency']))
        print("RRC Max Setup Time - HPA (ms): {}".format(group['ConnEstabTimeMaxHighPriorityAccess']))
        print("RRC Max Setup Time - MT (ms): {}".format(group['ConnEstabTimeMaxMtAccess']))
        print("RRC Max Setup Time - MO Signalling (ms): {}".format(group['ConnEstabTimeMaxMoSignalling']))
        print("RRC Max Setup Time - MO Data (ms): {}".format(group['ConnEstabTimeMaxMoData']))
        print("RRC Max Setup Time - Delay Tolerant (ms): {}".format(group['ConnEstabTimeMaxDelayTolerantAccess']))
